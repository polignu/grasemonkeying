// ==UserScript==
// @name           jupiterplus
// @description    Melhora a visualizacao da grade horaria no site Jupiter USP
// @include        https://uspdigital.usp.br/jupiterweb/*
// ==/UserScript==

//
// (c)2008 Felipe Sanches <juca@members.fsf.org>
// (c)2012 Diego Rabatone Oliveira <diraol@diraol.eng.br>
// This program is licensed under the GNU General Public License version 3 or later.
//

//Este script foi atualizado em 20 de Março de 2008

LARGURA=100
ALTURA=40

function timestr_to_pixels(str){
    var v = str.split(":");
    var res;
    res = parseFloat(v[0]) + parseFloat(v[1])/60 - 6.5
    return Math.floor(ALTURA * res);
}

cor_da_materia = {}
cor_usada = {}

//recebe valor numerico e retorna codigo de cor no formato "#rgb"
function cor(str){
    if (cor_da_materia[str])
        return cor_da_materia[str];

    var valor;
    do{
        valor = Math.floor(Math.random()*27);
    } while(cor_usada[valor]);
    cor_usada[valor] = true;

        // se vc mudar esses números vc terá uma paleta de cores diferente:
    var codigo = new Array;
    codigo[0] = "a";
    codigo[1] = "c";
    codigo[2] = "e";

    var ret = "";
    for (var i=0; i<3; i++){
        ret = codigo[valor % 3] + ret;
        valor = Math.floor(valor/3);
    }

    cor_da_materia[str] = "#" + ret
    return "#" + ret;
}

body = document.getElementsByTagName("body")[0];
nova_grade = document.createElement('div');
nova_grade.style.position = "relative";
nova_grade.style.fill = "true";
nova_grade.style.border = "thin solid";
nova_grade.style.height = timestr_to_pixels("18:50") - timestr_to_pixels("6:30");
nova_grade.style.width = LARGURA * 6.2;

function formata_disciplina(html){

    return html;
}

function adiciona_evento(dia, i,f,h){
    adiciona_div(dia, i,f,h, cor(h), true)
}

function adiciona_div(dia, i,f,h, cor, indicar_hora){
    var i_px = timestr_to_pixels(i);
    var f_px = timestr_to_pixels(f);
    var mydiv = document.createElement('div');
    if(indicar_hora)
        mydiv.innerHTML = "<font size=1>"+i+"</font>" + h
    else
        mydiv.innerHTML = h
    mydiv.style.width = 0.8 * LARGURA;
    mydiv.style.position = "absolute";
    mydiv.style.top = i_px;
    mydiv.style.left = LARGURA * (dia + 0.2);// + i_px/10;
    mydiv.style.height = f_px - i_px;
    mydiv.style.border = "thin solid";
    mydiv.style.backgroundColor = cor;

    nova_grade.appendChild(mydiv);
}

function tracejado_horas(i,f){
    for (var h=i;h<=f;h++){
        var mydiv = document.createElement('div');
        mydiv.style.heigth="1px";
        mydiv.style.border = "thin dashed #ddd";
        mydiv.style.left = LARGURA * 0.1;
        mydiv.style.top = timestr_to_pixels(h+":00");
        mydiv.style.width = LARGURA * 6;
        mydiv.style.position = "absolute";
        nova_grade.appendChild(mydiv);
    }
}

dia = ["Segunda", "Terca", "Quarta", "Quinta", "Sexta", "Sabado"];
for (var i=0; i<6; i++){
    adiciona_div(i, "6:40", "7:20", "<small><center>"+dia[i]+"</center></small>", "#EEE", false);
}
tracejado_horas(8,18);

encontrado = false;

tables = document.getElementsByTagName("table");
for (var i=0; i<tables.length;i++){
    tags_tr = tables[i].getElementsByTagName("tr")[0];
    tags_td = tags_tr.getElementsByTagName("td")[0];
    if (tags_td.innerHTML.indexOf("Horário")>0){
        tabela_original = tables[i]
        encontrado = true;
    }
}

if (encontrado){
    linhas = tabela_original.getElementsByTagName("tr");

    for (var i=1; i<linhas.length;i++){
        colunas = linhas[i].getElementsByTagName("td");
        inicio = colunas[0].firstChild.firstChild.innerHTML;
        fim = colunas[1].firstChild.firstChild.innerHTML;
        for (var j=2; j<=7;j++){
            html = colunas[j].firstChild.firstChild.innerHTML;
            if (html.indexOf("-")>0) adiciona_evento(j-2, inicio, fim, formata_disciplina(html));
        }
    }


    pai = tabela_original.parentNode;
    pai.replaceChild(nova_grade, tabela_original);
}
